#!/bin/bash
/code/manage.py makemigrations
/code/manage.py migrate
SUB='lbstorck-testing'
if [[ "$STAGING_HOST" == *"$SUB"* ]];
then
  uwsgi --show-config --http 0.0.0.0:8000
else
  uwsgi --show-config --http 0.0.0.0:80 --https 0.0.0.0:443,foobar.crt,foobar.key
fi

