.. storck documentation master file, created by
   sphinx-quickstart on Thu Mar  3 12:56:09 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Storck's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:

   Quick Start <quick_start.rst>
   Tutorial <tutorial.rst>
   REST API <rest_api.rst>
   Python Client <python_client>
   New Web UI <web_ui.rst>
   Running New Instance <running.rst>
   GitLab Repository <https://gitlab.cern.ch/velo-calibration-software/storck>


Storck is a database service, oriented on data versioning and accesibillity.
The core idea behind storck is to limit ability of the users for permament data alternation - be that a deletion of the data or change of it's content.

Storck itself is a service based on Djang REST API. Although one does not need to know the details of implementation to be able to interact with the database, the details about it's working phillosophy are crutial for utilising all it's advantages.

First thing that you should check out is :doc:`quick start<quick_start>` guide that will teach you the basic do's and dont's. Next head over to :doc:`tuorial<tutorial>` and further pages for extended details.

When you do that, then the simplest way to operate the storck is using the :doc:`web interface<web_ui>` (or :doc:`the old web interface<old_web_ui>`).

The other could be by using the :doc:`python storck client<python_client>` package, which provides object oriented Python level API for interacting with the database.

All of the options above at their core use the :doc:`REST API<rest_api>`, which can be used in any programming environment that supports http requests.

.. image:: https://i.imgur.com/BcxBP8h.png


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
