from django.urls import path

from .views import SearchFileView, FileView, InfoView, DirectoryView

urlpatterns = [
    path('search', SearchFileView.as_view(), name="all-files"),
    path('info', InfoView.as_view(), name="info-file"),
    path('file', FileView.as_view(), name="single-file"),
    path('directories', DirectoryView.as_view(), name="directories")
]
