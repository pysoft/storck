from django.db import models
from django.contrib.auth.models import User
from simple_history.models import HistoricalRecords


class Workspace(models.Model):
    users = models.ManyToManyField(User)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class WorkspaceToken(models.Model):
    workspace = models.ForeignKey(Workspace, on_delete=models.CASCADE, unique=True)
    token = models.CharField(max_length=100)
    name = models.CharField(max_length=100, default="")



class MetadataSchemas(models.Model):
    id = models.AutoField(primary_key=True)
    workspace = models.ForeignKey(Workspace, on_delete=models.CASCADE)
    schema = models.JSONField(default=None, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    filetype = models.TextField(default=None, unique=True)
    date = models.DateTimeField(auto_now_add=True)
    history = HistoricalRecords()
