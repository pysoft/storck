from rest_framework import serializers
from .models import MetadataSchemas

class MetadataSchemaSerializer(serializers.ModelSerializer):
    class Meta:
        model = MetadataSchemas
        fields = ['id', 'workspace', 'schema', 'user', 'filetype', 'date']
