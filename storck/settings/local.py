import os
dirname = os.path.dirname(os.path.abspath(__file__))

from .settings import *  # noqa: F403

DEBUG = True
ALLOWED_HOSTS = ['localhost']
DATABASES['default'] = {
    'ENGINE': 'django.db.backends.postgresql_psycopg2',
    'NAME': 'postgres',
    'USER': 'postgres',
    'PASSWORD': '',
    'HOST': 'localhost',
    'PORT': '5432',
}
CORS_ORIGIN_ALLOW_ALL = False
CORS_ORIGIN_WHITELIST = [
  'http://localhost:4200'
]