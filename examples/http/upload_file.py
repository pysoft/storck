import requests
import json

# THOSE ARE TEST CREDENTIALS.
# NEVER USE THEM OUTSIDE EXAMPLES!!!
workspace_token = "73f19f87-f741-4f53-ae50-a9272dc87ea7"
user_token = "7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be"


def upload_file():
    local_file_path = "../exampleFile.txt"
    filename = "exampleFile.txt"
    files = {"file": open(local_file_path, "rb")}
    meta_str =  json.dumps({"testproperty":"testvalue"})
    content = requests.post(
        "http://localhost:8000/api/file",
        params={"token": workspace_token},
        data={"path": filename, "metadata":meta_str},
        files=files,
        headers={"Authorization": "Token " + user_token},
    )
    print("--------------REQUEST (HTTP)----------------")
    print(content.request.url)
    print(content.request.body)
    print(content.request.headers)
    print()
    print("--------------RESPONSE (JSON)----------------")
    print(content.json())
    print()
    print("--------------RESPONSE----------------")
    print(str(content.headers))
    print(str(content.content))
    print("--------------------------------------")
    print(str(content.request.body))
    if content.status_code == 200:
        print("File uploaded successfully")
    else:
        raise ValueError


if __name__ == "__main__":
    upload_file()
