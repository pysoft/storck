#!/usr/bin/env bash

cd examples/http
set -e
python prepare_example_credentials.py -y
python upload_file.py
python auth_user.py
python create_workspace.py
python info_by_path.py
python info.py
python get_file.py
python search.py
python get_workspaces.py
python search_by_query.py
python metadata_schema.py
python upload_local_path.py
python download_local_path.py

cd ../client/
source env.sh
python -m pip install git+https://gitlab.cern.ch/velo-calibration-software/storck_client
python storck_client_test.py
