from django.urls import path
from .views import main, logout

urlpatterns = [
    path('main', main, name='main-view'),
    path('logout', logout, name='logout')
]
