import { uploadFile } from "./requests.js";

let dropArea = document.getElementById('dropArea');
let filesToUpload = document.getElementById('filesToUpload');
let chooseFile = document.getElementById('chooseFiles');

const highlight = (e) => dropArea.classList.add('highlight');
const unhighlight = (e) => dropArea.classList.remove('highlight');

const preventDefaults = (e) => {
    e.preventDefault();
    e.stopPropagation();
};

const UploadItem = (file) => {
    const itemWrapper = document.createElement("li");
    const filename = document.createElement("span");
    const metaInput = document.createElement("input");
    const nameInput = document.createElement("input");
    const metaLabel = document.createElement("label");
    const nameLabel = document.createElement("label");
    const inputsWrapper = document.createElement("div");
    const metaSection = document.createElement("div");
    const nameSection = document.createElement("div");

    itemWrapper.classList.add('uploadItem');
    inputsWrapper.classList.add('uploadItemInputs');
    metaSection.classList.add('uploadItemSection');
    nameSection.classList.add('uploadItemSection');

    filename.innerHTML = file.file.name;
    metaLabel.innerHTML = "Meta data";
    nameLabel.innerHTML = "File name";
    nameInput.value = file.file.name;

    nameInput.onchange = (e) => {
        file.name = e.target.value;
    };
    metaInput.onchange = (e) => {
        file.meta = e.target.value;
    };

    metaSection.appendChild(metaLabel);
    metaSection.appendChild(metaInput);
    nameSection.appendChild(nameLabel);
    nameSection.appendChild(nameInput);

    inputsWrapper.appendChild(metaSection);
    inputsWrapper.appendChild(nameSection);

    itemWrapper.appendChild(filename);
    itemWrapper.appendChild(inputsWrapper);

    return itemWrapper;
};

let FILES = [];

const handleDrop = async (e) => {
    const files = (e.dataTransfer && e.dataTransfer.files) || chooseFile.files;
    const transferredFiles = [...files];
    FILES = [
        ...FILES,
        ...transferredFiles.map((file) => ({ file })),
    ];

    filesToUpload.innerHTML = null;
    FILES.forEach((file) => {
        filesToUpload.appendChild(UploadItem(file));
    });
};

const handleUpload = async () => {
     await Promise.all(
         FILES.map((file) => uploadFile(file))
     );

     window.location.reload();
};

['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => dropArea.addEventListener(eventName, preventDefaults, false));
['dragenter', 'dragover'].forEach(eventName => dropArea.addEventListener(eventName, highlight, false));
['dragleave', 'drop'].forEach(eventName => dropArea.addEventListener(eventName, unhighlight, false));
dropArea.addEventListener('drop', handleDrop, false);
chooseFile.addEventListener('change', handleDrop);

window.handleDrop = handleDrop;
window.handleUpload = handleUpload;
